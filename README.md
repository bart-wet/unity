This is a simple way of implementing motion in Unity with 6 degrees of freedom.
With this script provides 3 rotation axes and 3 translation movements. Each
of these movements are described relative to the main camera and not to the
world axes.

Usage of these files
======================================

These files can be added to a project without any movement in the main camera yet.
The following steps assume that Unity is currently not running. If it is, please
close it.

Step 1: 
- Copy the script "CameraMovement.cs" into your project's Assets folder

Step 2:
- Open the file "{ProjectBase}\ProjectSettings\InputManager.asset" with a 
  text editor (NOT unity)
- Copy the contents of the file "Inputmanager.asset.yml" into the file
  "InputManager.asset", make sure to maintain the yaml structure of the file
  My advice is to copy the new inputs at the end of the file
  
Step 3:
- Open your Unity project in Unity
- Add the script "CameraMovement.cs" as a C# Script component to the "Main Camera"

Step 4:
- Build the project to see the effects

Movement provided by this script
======================================

This script gives the following degrees of translation:
- Forward:          W
- Backwards:        S
- Right:            Q     
- Left:             E
- Up:               R
- Down:             F

This script gives the following degrees of rotation:
- Yaw Left:         A
- Yaw Right:        D
- Pitch Up:         Down     
- Pitch Down:       Up
- Roll Left:        Left
- Roll Right:       Right

The speed of translation can be set using the following keys:
- No Speed:         F1
- 25% of maximum:   F2
- 50% of maximum:   F3
- 75% of maximum:   F4
- 100% of maximum:  F5
- Accelerate:       Numpad Plus
- Decelerate:       Numpad Minus
- Speed boost:      Shift

The way the script works
======================================

This script uses the inputs as defined in the "Input" part of the 
"Project Settings". The keys bound for that movement can be redefined in that
part. If you don't want to use a certain axis of input, it still need to 
exist in the "Input" part of the "Project Settings". The easiest way to disable 
them is to remove any keybindings.

The keys of the speed are defined in the script itself. The default speed is
hardcoded to 10 with a maximum speed of 100, and the "Speed boost" quadruples
the set speed by 4.

Each fixed update the Main camera is moved for "speed" units, if one or more
of the movement keys is pressed. The rotation is about 1 full rotation per 
10 seconds.

