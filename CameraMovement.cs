using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    private readonly Dictionary<KeyCode, Func<int, int>> speedKeys = new Dictionary<KeyCode, Func<int, int>>();
    private int speed = 10;
    private int maxSpeed = 100;

    void Start()
    {
        speedKeys.Add(KeyCode.KeypadMinus, (s) => s == 0 ? s : s - 1);
        speedKeys.Add(KeyCode.KeypadPlus, (s) => s == maxSpeed ? s : s + 1);
        speedKeys.Add(KeyCode.F1, (s) => 0);
        speedKeys.Add(KeyCode.F2, (s) => maxSpeed / 4);
        speedKeys.Add(KeyCode.F3, (s) => maxSpeed / 2);
        speedKeys.Add(KeyCode.F4, (s) => maxSpeed * 3 / 4);
        speedKeys.Add(KeyCode.F5, (s) => maxSpeed);
    }

    void FixedUpdate()
    {
        foreach (KeyCode key in speedKeys.Keys)
        {
            if (Input.GetKey(key))
            {
                speed = speedKeys[key].Invoke(speed);
                break;
            }
        }

        int tempSpeed = speed;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            tempSpeed *= 4;
        }

        Vector3 moveDirection = new Vector3(
            Input.GetAxis("Left-Right") * tempSpeed,
            Input.GetAxis("Up-Down") * tempSpeed,
            Input.GetAxis("Forward-Backward") * tempSpeed
        );

        Vector3 rotateDirection = new Vector3(
            Input.GetAxis("Pitch"),
            Input.GetAxis("Yaw"),
            Input.GetAxis("Roll")
        );

        GetComponentInParent<Transform>().Translate(moveDirection);
        GetComponentInParent<Transform>().Rotate(rotateDirection);
    }

}
